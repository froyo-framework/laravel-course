@extends('layouts.main')

@section('title') Page @stop

@section('content')
  <h1>{{ $title }}</h1>
  <p>{{ $description }}</p>
@stop