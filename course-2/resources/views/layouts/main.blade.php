<!doctype html>
<html>
  <head>
    <title>@yield('title')</title>
  </head>
  <body>
    <navbar>
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/page">Page</a></li>
      </ul>
    </navbar>
    
    @yield('content')

  </body>
</html>